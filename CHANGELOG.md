# Change Log

All notable changes to this project will be documented in this file.
Also this is something like roadmap :D
 
>The format is based on [Keep a Changelog](http://keepachangelog.com/)
>
>and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased / To be done] - ????-??-??

Features update WIP

### Added
- New **/status** command, to show bot info in telegram chat
- Now can edit messages, sent by bot via UI

### Changed

- Logs fully reworked

## [4.3] - 2022-03-24

QoL Update

### Added

- New **/subscribe** command. Now bot admins can add group/chat to subscribed list, so this group/chat will receive all updates with track info.
- New **/listen** command. Now bot admins can start bot to listen radioboss changes that will be sent in all subscribed chats.
- New admin setting field, where you can set users that can use *admin* commands, later will be reworked.

### Changed

- Whole UI and Backend code rewritten in a better way again

### Removed
- Setting field with chatid
- **/chatid** command no longer needed.


## [4.2] - 2022-03-02

UI Overhaul update p2/2
Moved whole UI stuff to MahApps Metro framework. As for me most flexible module that allows to create nice UI.

### Added

- Log export button

### Changed

- Whole UI and Backend code rewritten in a better way again
- No more day/month/year in logs, instead it now writes type of log.

### Fixed

- From NOW!!! Every sent message should be handled correctly, so no duplicates/errors/crashes/nuklear wars should appear.
- **/tracklist** command crashed whole app in some cases :\ no it shouldn't

## [4.1] - 2022-02-24

UI Overhaul update p1/2
All views and viewmodels rewritten from scratch

### Added

- Now bot status is monitored in bottom bar
- New **/tracklist** command, that shows you current tracklist played on radio

### Changed

- Logs fully reworked
- Whole UI and Backend code rewritten in a better way
- Now settings stored in SQLiteDB

### Fixed

- Minor and major crashes
- Several methods now work asyncronous to prevent freezes

## [2.7.0] - 2021-08-05

This release focused on bugfixes and other crap that didn't worked properly

### Fixed

- Sending messages after restarting listening to radioboss now should work correctly
- Deleting message method improved
- Crashes with NullReferenceException
- Not sending messages without photo

### Changed

- Moved back to Telegram.Bot api again, because of its more stable than Telegram.BotAPI
- Reworked GetUpdate handling

## [2.6.2] - 2021-07-27

### Fixed

- Potentially fixed duplicated messages and crashes

### Changed

- Now config stores backup file in separated folder and provides config file name with date when backup been made
- Refactored some methods name, removed several redundant code parts.

## [2.6.1] - 2021-07-25

### Added

- Praying code to save bot from crashes

### Fixed

- Unkown reason crash
- Minor fixes

## [2.6.0] - 2021-07-25

This version contains a lot of bugfixes and some refactoring.

### Added

- Messages update when listener amount being changed.
- Formatting next track as current track

### Changed

- Now, when you stop RadioBoss listening or reloading/saving config, latest sent messages are removed to prevent chat flooding.
- Reworked message reveive/update method

### Fixed

- Save config button crash
- Stop listening button stops bot listening immediatly instead of waiting existing Task.Delays
- Crash when invalid data provided in Radio Boss settings
- Fixed "token visibility" toggle button
- Other minor bugs

## [2.5.0] - 2021-07-15

Just moving to another telegram bot API
 
### Changed

- Moved to Telegram.BotAPI instead of Telegram.Bot
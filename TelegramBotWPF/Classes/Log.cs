﻿using System;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;

#nullable enable

namespace TelegramBotWPF.Classes
{
    internal enum LogType
    {
        Info,
        Warn,
        Error
    }

    public class LogEventArgs
    {
        public LogEventArgs(string text)
        {
            Text = text;
        }

        public string Text { get; }
    }

    internal static class LogUtil
    {
        public delegate void LogEventHandler(LogEventArgs e);

        public static event LogEventHandler? OnLog;

        private static string _logString = string.Empty;
        public static string LastLine = string.Empty;

        public static string LogFile => @$"{DateTime.Now:dd.MM.yyyy_HHmmss}.log";

        public static async void Log(string s, LogType logtype = LogType.Info)
        {
            await Task.Run(() =>
            {
                s = $"[{DateTime.Now.ToString("HH:mm:ss", new CultureInfo("en-EN"))} {logtype}] " + s + "\n";
                LastLine = s;
                _logString += s;
                OnLog?.Invoke(new LogEventArgs(s));
            });
        }

        public static string GetLogText()
        { return _logString; }

        public static void ExportLogToFile()
        {
            File.AppendAllText(LogFile, _logString);
            Log("Exported log to " + LogFile);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InputFiles;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramBotWPF.Models;
using static TelegramBotWPF.Classes.LogUtil;
using static TelegramBotWPF.Models.JsonModel;
using SysFile = System.IO.File;

#nullable enable

namespace TelegramBotWPF.Classes
{
    internal class Bot
    {
        public delegate void ChangedBotStatus(string runStatus);

        public static event ChangedBotStatus? UpdateStatus;

        public bool ShouldStopStream = true;
        private int ErrCount = 0;
        private bool? _isArtWorkOld;
        private string _audioStreamUrl = string.Empty;
        private TelegramBotClient? _botClient;
        private Dictionary<string, bool> _chatList = new();
        private readonly Dictionary<string, int> _deleteInfo = new();
        private User _botInfo = new();
        private RadioBossModel _rbData = new(false);
        private CancellationTokenSource _cts = new();
        private string _messageToSendOld = string.Empty;
        private string _messageToSendNew = string.Empty;

        private InlineKeyboardMarkup Keyboard => new(InlineKeyboardButton.WithUrl($"{TransformMessage(JsonLanguage.ListenUrlText)}", _audioStreamUrl));

        public bool IsBot;

        private readonly List<BotCommand> _cmdList = new()
        {
            new BotCommand { Command = "listen", Description = "Start/stop sending messages." },
            new BotCommand { Command = "subscribe", Description = "Adds/removes chat from listening to messages" },
            new BotCommand { Command = "status", Description = "Shows bot status" },
            new BotCommand { Command = "tracklist", Description = "Writes all tracks that are in current playlist" }
        };

        private string TransformMessage(string s)
        {
            return s
                    .Replace(MessagePrefix.currTrack, _rbData.CurrentTrack!.Name)
                    .Replace(MessagePrefix.currTrackLen, _rbData.CurrentTrack.Duration)
                    .Replace(MessagePrefix.nextTrackDate, _rbData.NextTrackDate())
                    .Replace(MessagePrefix.nextTrack, _rbData.NextTrack!.Name)
                    .Replace(MessagePrefix.nextTrackLen, _rbData.NextTrack.Duration)
                    .Replace(MessagePrefix.listeners, _rbData.NowListening)
                    .Replace(MessagePrefix.radioName, JsonLanguage.RadioName)
                    .Replace(MessagePrefix.genre, _rbData.CurrentTrack.Genre)
                    .Replace(MessagePrefix.nextgenre, _rbData.NextTrack.Genre)
                    .Replace(MessagePrefix.comment, _rbData.CurrentTrack.Description)
                    .Replace(MessagePrefix.nextcomment, _rbData.NextTrack.Description);
        }

        private static string? GetChatTitle(Message msg)
        {
            return msg.Chat.Title ?? msg.Chat.Username;
        }

        private async Task SendFromRadioBoss()
        {
            if (ShouldStopStream || _botClient == null) { return; }
            var isArtWork = _rbData.ArtWork is { Length: > 0 };
            _isArtWorkOld ??= isArtWork;
            await UpdateChatList();
            var lastChat = string.Empty;
            try
            {
                foreach (var chat in _chatList.Keys)
                {
                    lastChat = chat;
                    Task<Message> msgInfoAwaiter;

                    if (_chatList[chat] || isArtWork != _isArtWorkOld)
                    {
                        await DeleteMessage(chat);
                        msgInfoAwaiter = isArtWork
                        ? _botClient.SendPhotoAsync(chat, new InputOnlineFile(_rbData.ArtWork), _messageToSendNew, disableNotification: true, replyMarkup: Keyboard)
                        : _botClient.SendTextMessageAsync(chat, _messageToSendNew, disableNotification: true, replyMarkup: Keyboard);
                        _chatList[chat] = false;
                        _deleteInfo.Add(chat, msgInfoAwaiter.Result.MessageId);
                        Log($"Created message({msgInfoAwaiter.Result.MessageId}) in ({GetChatTitle(msgInfoAwaiter.Result)} | {msgInfoAwaiter.Result.Chat.Id})");
                    }
                    else
                    {
                        msgInfoAwaiter = isArtWork
                        ? _botClient.EditMessageMediaAsync(chat, _deleteInfo[chat], new InputMediaPhoto(new InputMedia(_rbData.ArtWork, "art")) { Caption = _messageToSendNew }, replyMarkup: Keyboard)
                        : _botClient.EditMessageTextAsync(chat, _deleteInfo[chat], _messageToSendNew, replyMarkup: Keyboard);
                        Log($"Updated track info ({GetChatTitle(msgInfoAwaiter.Result)} | {msgInfoAwaiter.Result.Chat.Id})");
                    }
                    msgInfoAwaiter.Wait();
                }
            }
            catch (Exception e)
            {
                Log($"Exception caught on sending RadioBoss message.\n>>>{e.Message}", LogType.Error);
                _chatList[lastChat] = true;
            }
            _isArtWorkOld = isArtWork;
        }

        private async Task RadioBossTrackCheck()
        {
            if (ShouldStopStream) { return; }
            await _rbData.RetrieveData();
            _messageToSendNew = TransformMessage(JsonLanguage.AudioMessage);
            if (_rbData.ConnectionValid)
            {
                if (_messageToSendOld == _messageToSendNew) return;
                //Sending message
                await SendFromRadioBoss();
                _messageToSendOld = _messageToSendNew; // Replacing strored message to check it later, if something being changed
                // Removing empty data from _deleteInfo
                var delData = _deleteInfo.Aggregate(string.Empty, (current, item) => current + $"{item.Key}+{item.Value}\n");
                await SysFile.WriteAllTextAsync("msgdata", delData);
                return;
            }
            Log("Failed to update RadioBoss data.", LogType.Error);
        }

        private async void RadioBossPing()
        {
            while (!ShouldStopStream)
            {
                await Task.Delay(15000);
                await RadioBossTrackCheck();
            }
        }

        /// <summary>
        /// Start listening to radioboss and sending messages if something changed
        /// </summary>
        public async Task StartRadioBossListening()
        {
            Log("Starting RadioBoss listener...");
            UpdateStatus?.Invoke("Preparing to start...");
            if (!_rbData.ConnectionValid)
            {
                Log("Trying to connect to RadioBoss server...");
                await _rbData.RetrieveData();
            }
            if (_rbData.ConnectionValid)
            {
                Log("Began listening to RadioBoss.");
                await RadioBossTrackCheck();
                RadioBossPing();
                UpdateStatus?.Invoke("Running...");
                return;
            }
            ShouldStopStream = true;
            Log("Unable to connect to Radio boss", LogType.Error);
            UpdateStatus?.Invoke("Stopped");
        }

        /// <summary>
        /// Stop from listening to radioboss updates
        /// </summary>
        public async Task StopRadioBossListening()
        {
            _messageToSendOld = string.Empty;
            await UpdateChatList();
            foreach (var chat in _chatList.Keys)
            {
                _chatList[chat] = true;
                await DeleteMessage(chat);
            }
            if (SysFile.Exists("msgdata")) { SysFile.Delete("msgdata"); }
            Log("Stopped listening to RadioBoss.");
            UpdateStatus?.Invoke("Stopped");
        }

        private async Task InitCommands()
        {
            await _botClient!.SetMyCommandsAsync(_cmdList);
        }

        private static bool CheckAdmin(string? username) => username != null && JsonConfig.Admins.Contains(username);

        private async void ListenCmd(Message? message)
        {
            if (_botClient == null || message?.From == null) { return; }

            if (!CheckAdmin(message.From.Username)) return;
            var id = message.Chat.Id.ToString();
            ShouldStopStream = !ShouldStopStream;
            if (ShouldStopStream)
            {
                await _botClient.SendTextMessageAsync(id, "Stopped listening...");
                Log($"{message.From.Username} stopped listening");
                await StopRadioBossListening();
            }
            else
            {
                await _botClient.SendTextMessageAsync(id, "Starting listening...");
                Log($"{message.From.Username} started listening");
                await StartRadioBossListening();
            }
        }

        private async void SubscribeCmd(Message? message)
        {
            if (_botClient == null || message?.From == null) { return; }
            if (CheckAdmin(message.From.Username))
            {
                string id = message.Chat.Id.ToString();
                List<string> idList = JsonConfig.ChatId.Split().Where(x => x != string.Empty).ToList();
                if (idList.Contains(id))
                {
                    idList.Remove(id);
                    Log($"Removed {id} from chat ids");
                    await _botClient.SendTextMessageAsync(id, "Removed this chat from receiving radio info.");
                }
                else
                {
                    idList.Add(id);
                    Log($"Added {id} to chat ids");
                    await _botClient.SendTextMessageAsync(id, "This chat will receive radio info.");
                }
                JsonConfig.ChatId = string.Join(" ", idList);
                SaveJson(JsonType.Cfg);
            }
        }

        private async void GetStatus(Message? message)
        {
            if (_botClient == null || message?.From == null) { return; }
            try
            {
                await _botClient.SendTextMessageAsync(message.From.Id, "work in progress");
                Log($"{message.From.Username} requested status");
            }
            catch (Exception e)
            {
                Log($"Exception on getting status >>>{e}");
            }
        }

        private async void GetTrackList(Message? message)
        {
            if (_botClient == null || message?.From == null) { return; }
            var curIndex = await _rbData.CurrentTrackIndex();
            List<Track> tracks = await _rbData.CurrentPlaylist();
            var msg = $"Current tracklist ({tracks.Count} tracks):\n\n";
            foreach (var track in tracks)
            {
                if (int.Parse(track.TrackIndex!) == curIndex)
                {
                    msg += "▶ ";
                }
                msg += $"{track.TrackIndex}. [{track.StartTime}] {track.Name} ({track.Duration})\n{track.Genre}\n\n";
                if (msg.Length <= 1000) continue;
                await _botClient.SendTextMessageAsync(message.Chat.Id, msg, disableNotification: true);
                msg = string.Empty;
            }
            if (msg.Length > 0)
            {
                await _botClient.SendTextMessageAsync(message.Chat.Id, msg, disableNotification: true);
            }
        }

        private void HandleCommand(string cmd, Update update)
        {
            if (_botInfo.Username != null && cmd.Contains('@', new StringComparison()))
            {
                if (cmd.EndsWith(_botInfo.Username, new StringComparison())) { cmd = cmd.Split('@')[0]; }
                else { return; }
            }
            switch (cmd)
            {
                case "listen":
                    ListenCmd(update.Message);
                    break;

                case "subscribe":
                    SubscribeCmd(update.Message);
                    break;

                case "status":
                    GetStatus(update.Message);
                    break;

                case "tracklist":
                    GetTrackList(update.Message);
                    break;
            }
        }

        private async Task HandleUpdate(ITelegramBotClient botClient, Update update, CancellationToken ct)
        {
            await Task.Run(() =>
            {
                if (update.Type != UpdateType.Message) return;
                _chatList[update.Message!.Chat.Id.ToString()] = true;
                var message = update.Message;
                if (message?.Text is null) { return; }

                if (!message.Text.StartsWith(@"/", new StringComparison())) return;
                HandleCommand(message.Text[1..], update);
            }, ct);
        }

        private static async Task HandleUpdateError(ITelegramBotClient botClient, Exception exception, CancellationToken ct) =>
            await Task.Run(() =>
            {
                Log($"Error occured on handling update\n>>>{exception.Message}", LogType.Error);
            }, ct);

        private async Task DeleteMessage(string chat)
        {
            try
            {
                if (_deleteInfo.ContainsKey(chat))
                {
                    if (_deleteInfo[chat] != 0 && _botClient != null)
                    {
                        await _botClient.DeleteMessageAsync(chat, _deleteInfo[chat]);
                        Log($"Message {_deleteInfo[chat]} removed from {chat}");
                    }
                }
            }
            catch (Exception e)
            {
                Log($"Cant delete message {_deleteInfo[chat]} from {chat}\n>>>{e.Message}", LogType.Error);
            }
            finally
            {
                _deleteInfo.Remove(chat);
            }
        }

        private async Task UpdateChatList()
        {
            await Task.Run(() =>
            {
                Dictionary<string, bool> result = new();
                foreach (var chatId in JsonConfig.ChatId.Split())
                {
                    result.Add(chatId, true);
                    if (_chatList.ContainsKey(chatId))
                    {
                        result[chatId] = _chatList[chatId];
                    }
                }
                _chatList = result;
            });
        }

        /// <summary>
        /// Stopping bot instance
        /// You better do this, before reinitializing bot instance
        /// </summary>
        public async Task StopBotInstance()
        {
            try
            {
                if (!IsBot || _botClient == null) { return; }
                await StopRadioBossListening();
                IsBot = false;
                _cts.Cancel();
                await _botClient.CloseAsync();
                Log("Stopped bot instance.");
            }
            catch (Exception e)
            {
                Log("Exception on stopping bot instance\n>>>" + e.Message, LogType.Error);
            }
        }

        /// <summary>
        /// Initializes bot instance with given parameters
        /// </summary>
        public async Task InitBot()
        {
            Log("Initializing bot instance");
            if (JsonConfig.BotToken is { Length: 0 })
            {
                Log("Waiting for bot token.", LogType.Warn);
                return;
            }
            _botClient = new TelegramBotClient(JsonConfig.BotToken);
            UpdateStatus?.Invoke("...");
            Log("Testing bot token...");
            if (_botClient == null)
            {
                Log("Token is incorrect.");
                return;
            }
            try
            {
                _botInfo = _botClient.GetMeAsync().Result;
                Log($"Token accepted.\n>>>Connected to the {_botInfo.FirstName}(@{_botInfo.Username})");
                IsBot = true;
                await InitCommands();
                _cts = new CancellationTokenSource();
                _botClient.StartReceiving(HandleUpdate, HandleUpdateError, null, _cts.Token);
                UpdateStatus?.Invoke("Idling");
            }
            catch
            {
                IsBot = false;
                Log("Invalid bot token. Waiting for valid one.", LogType.Warn);
            }

            _audioStreamUrl = JsonConfig.AudioStreamUrl;
            _rbData = new RadioBossModel();
            _deleteInfo.Clear();
            if (SysFile.Exists("msgdata"))
            {
                await UpdateChatList();
                var data = await SysFile.ReadAllTextAsync("msgdata");
                try
                {
                    foreach (var pair in data.Split("\n"))
                    {
                        if (pair == string.Empty) continue;

                        _deleteInfo.Add(pair.Split("+")[0], int.Parse(pair.Split("+")[1]));
                    }
                    if (_deleteInfo.Count > 0)
                    {
                        Log($"Trying to remove old messages({_deleteInfo.Count})");
                        foreach (var chat in _chatList.Keys)
                        {
                            await DeleteMessage(chat);
                        }
                        Log("All possible messages being removed");
                    }
                }
                catch (Exception e)
                {
                    Log($"Something happened while removing old messages:\n>>>{e.Message}", LogType.Error);
                }
                SysFile.Delete("msgdata");
            }
        }
    }
}
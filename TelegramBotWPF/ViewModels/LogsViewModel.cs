﻿using Microsoft.Toolkit.Mvvm.ComponentModel;
using TelegramBotWPF.Classes;
using static TelegramBotWPF.Classes.LogUtil;

namespace TelegramBotWPF.ViewModels
{
    internal class LogsViewModel : ObservableObject
    {
        public LogsViewModel()
        {
            LogContent = GetLogText();
            OnLog += UpdateLog;
        }

        private void UpdateLog(LogEventArgs e)
        {
            LogContent += e.Text;
        }

        private string _logContent = "";
        public string LogContent { get => _logContent; set => SetProperty(ref _logContent, value); }
    }
}
﻿using Microsoft.Toolkit.Mvvm.ComponentModel;
using Microsoft.Toolkit.Mvvm.Input;
using System.Threading.Tasks;
using TelegramBotWPF.Classes;
using TelegramBotWPF.Models;
using static TelegramBotWPF.Models.JsonModel;
using static TelegramBotWPF.Classes.LogUtil;

namespace TelegramBotWPF.ViewModels
{
    internal class SettingsViewModel : ObservableObject
    {
        public SettingsViewModel()
        {
            SendButtonText = "Start sending audio info";
            LoadButton = new AsyncRelayCommand(LoadButtonMethod);
            SendCurrTrack = new AsyncRelayCommand(SendCurrTrackMethod);
            SaveButton = new AsyncRelayCommand(SaveButtonMethod);
            ResetChats = new AsyncRelayCommand(ResetChatsMethod);
            IsBot = false;
            Parallel.Invoke(() => { _ = InitializeBot(); });
        }

        public IAsyncRelayCommand LoadButton { get; }
        public IAsyncRelayCommand ResetChats { get; }
        public IAsyncRelayCommand SendCurrTrack { get; }
        public IAsyncRelayCommand SaveButton { get; }
        public string BotToken { get => JsonConfig.BotToken; set => SetProperty(ref JsonConfig.BotToken, value); }
        public string Admins { get => JsonConfig.Admins; set => SetProperty(ref JsonConfig.Admins, value); }
        public string RadioBossUrl { get => JsonConfig.RadioBossUrl; set => SetProperty(ref JsonConfig.RadioBossUrl, value); }
        public string RadioBossPass { get => JsonConfig.RadioBossPass; set => SetProperty(ref JsonConfig.RadioBossPass, value); }
        public string AudioStreamUrl { get => JsonConfig.AudioStreamUrl; set => SetProperty(ref JsonConfig.AudioStreamUrl, value); }
        public bool ShouldAutoStart { get => JsonConfig.ShouldAutoStart; set => SetProperty(ref JsonConfig.ShouldAutoStart, value); }

        private bool _canSaveLoad;
        public bool CanSaveLoad { get => _canSaveLoad; set => SetProperty(ref _canSaveLoad, value); }
        private string _sendButtonText = "";
        public string SendButtonText { get => _sendButtonText; set => SetProperty(ref _sendButtonText, value); }
        private bool _isBot;
        public bool IsBot { get => _isBot; set => SetProperty(ref _isBot, value); }
        private readonly Bot _tgBot = new();

        private static async Task ResetChatsMethod()
        {
            Log("Resetting all chat id's that being subscribed.");
            await Task.Run(() =>
            {
                JsonConfig.ChatId = string.Empty;
                SaveJson(JsonType.Cfg);
            });
        }

        private async Task SendCurrTrackMethod()
        {
            _tgBot.ShouldStopStream = !_tgBot.ShouldStopStream;
            if (_tgBot.ShouldStopStream)
            {
                SendButtonText = "Start sending audio info";
                await _tgBot.StopRadioBossListening();
                return;
            }
            SendButtonText = "Stop sending audio info";
            await _tgBot.StartRadioBossListening();
        }

        private async Task SaveButtonMethod()
        {
            CanSaveLoad = false;
            SaveJson(JsonType.Cfg);
            await InitializeBot();
        }

        private async Task LoadButtonMethod()
        {
            CanSaveLoad = false;
            LoadJson(JsonType.Cfg);
            await InitializeBot();
        }

        private async Task InitializeBot()
        {
            if (IsBot)
            {
                await _tgBot.StopBotInstance();
            }
            await _tgBot.InitBot();
            IsBot = _tgBot.IsBot;

            if (ShouldAutoStart && IsBot)
            {
                await SendCurrTrackMethod();
            }
            CanSaveLoad = true;
        }
    }
}
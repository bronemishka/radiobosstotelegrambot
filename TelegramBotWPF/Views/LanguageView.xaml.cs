﻿using System.Windows;
using TelegramBotWPF.ViewModels;

namespace TelegramBotWPF.Views
{
    /// <summary>
    /// Interaction logic for LanguageView.xaml
    /// </summary>
    public partial class LanguageView
    {
        public LanguageView()
        {
            InitializeComponent();
            DataContext = new LanguageViewModel();
            InProgressStack.Visibility = Visibility.Visible;
            MainGrid.Visibility = Visibility.Collapsed;
        }

        private void ProgressBar_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key != System.Windows.Input.Key.O) return;
            InProgressStack.Visibility = Visibility.Collapsed;
            MainGrid.Visibility = Visibility.Visible;
        }
    }
}
﻿using System.Windows;
using System.Windows.Controls;
using TelegramBotWPF.ViewModels;
using static TelegramBotWPF.Classes.LogUtil;

namespace TelegramBotWPF.Views
{
    /// <summary>
    /// Interaction logic for LogsView.xaml
    /// </summary>
    public partial class LogsView
    {
        public LogsView()
        {
            InitializeComponent();
            DataContext = new LogsViewModel();
        }

        private void ExportLog(object sender, RoutedEventArgs e)
        {
            ExportLogToFile();
        }

        public void UpdateCaret(object sender, TextChangedEventArgs e)
        {
            LogBox.ScrollToEnd();
        }
    }
}
﻿using System.Reflection;
using TelegramBotWPF.Classes;
using TelegramBotWPF.ViewModels;
using static TelegramBotWPF.Classes.Bot;
using static TelegramBotWPF.Classes.LogUtil;
using static TelegramBotWPF.Models.JsonModel;

namespace TelegramBotWPF.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            OnLog += UpdateLogStatusBar;
            UpdateStatus += UpdateBotStatusBar;
            Log("Preparing json configs...");
            LoadJson(Models.JsonType.Lang);
            LoadJson(Models.JsonType.Cfg);
            Log("Done!");
            Log("Initializing UI and other stuff...");
            InitializeComponent();
            DataContext = new MainWindowViewModel();
            var version = Assembly.GetExecutingAssembly().GetName().Version;
            var assemblyname = Assembly.GetExecutingAssembly().GetName().Name;
            Title = $"{assemblyname} {version?.Major}.{version?.Minor} (Build: {version?.Build} Rev: {version?.Revision})";
        }

        private void UpdateBotStatusBar(string status)
        {
            Dispatcher.BeginInvoke(() =>
            {
                BotStatus.Content = $"Status: {status}";
            });
        }

        private void UpdateLogStatusBar(LogEventArgs e)
        {
            Dispatcher.BeginInvoke(() =>
            {
                LastLog.Content = e.Text.Split("\n")[0];
            });
        }
    }
}
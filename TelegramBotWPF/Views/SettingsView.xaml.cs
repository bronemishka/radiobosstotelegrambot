﻿using System.Windows;
using System.Windows.Controls;
using TelegramBotWPF.ViewModels;

namespace TelegramBotWPF.Views
{
    /// <summary>
    /// Interaction logic for SettingsView.xaml
    /// </summary>
    public partial class SettingsView : UserControl
    {
        public SettingsView()
        {
            InitializeComponent();
            DataContext = new SettingsViewModel();
            TokenIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.Show;
            PassIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.Show;
        }
        private void ShowHideToken(object s, RoutedEventArgs e)
        {
            if (TokenBox.Visibility == Visibility.Collapsed) // Hide password if visible
            {
                TokenBox.Visibility = Visibility.Visible;
                TokenBoxText.Visibility = Visibility.Collapsed;
                TokenIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.Show;
                return;
            }
            if (TokenBox.Visibility == Visibility.Visible) // Show password if hidden
            {
                TokenBox.Visibility = Visibility.Collapsed;
                TokenBoxText.Visibility = Visibility.Visible;
                TokenIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.Hide;
                return;
            }
        }
        private void ShowHidePass(object s, RoutedEventArgs e)
        {
            if (PassBox.Visibility == Visibility.Collapsed) // Hide password if visible
            {
                PassBox.Visibility = Visibility.Visible;
                PassBoxText.Visibility = Visibility.Collapsed;
                PassIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.Show;
                return;
            }
            if (PassBox.Visibility == Visibility.Visible) // Show password if hidden
            {
                PassBox.Visibility = Visibility.Collapsed;
                PassBoxText.Visibility = Visibility.Visible;
                PassIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.Hide;
                return;
            }
        }
    }
}
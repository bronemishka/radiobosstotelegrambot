﻿using Newtonsoft.Json;
using System;
using System.IO;
using TelegramBotWPF.Classes;
using static TelegramBotWPF.Classes.LogUtil;

namespace TelegramBotWPF.Models
{
    [JsonObject(MemberSerialization.Fields)]
    public class JsonLang
    {
        public string RadioName = "Vandal FM";
        public string AudioMessage = "На %radioName% 🔊\n\n▶ Сейчас:\n%currTrack% [%currTrackLen%]\n%genre%\nИнфо: %comment%\n\n⏭ Следующий в %nextTrackDate% MSK\n%nextTrack% [%nextTrackLen%]\n%nextgenre%\nИнфо: %nextcomment%\n\n👤 Слушателей: %listeners%";
        public string StatusMessage = string.Empty;
        public string ListenUrlText = "Слушать %currTrack% на %radioName%";
    }

    [JsonObject(MemberSerialization.Fields)]
    public class JsonSettings
    {
        public string BotToken = string.Empty;
        public bool ShouldAutoStart;
        public string RadioBossUrl = string.Empty;
        public string RadioBossPass = string.Empty;
        public string ChatId = string.Empty;
        public string AudioStreamUrl = string.Empty;
        public string Admins = string.Empty;
    }


    public enum JsonType
    {
        Cfg,
        Lang
    }

    /// <summary>
    /// Model that provide application with settings via Json
    /// </summary>
    public static class JsonModel
    {
#pragma warning disable CA2211 // Non-constant fields should not be visible
        private const string CfgPath = @"";
        private const string CfgFile = @"config.json";
        private static string CfgFullPath => Path.Combine(CfgPath + CfgFile);
        public static string LangPath = @"lang/";
        public static string LangFile = @"en.json";
        private static string LangFullPath => Path.Combine(LangPath + LangFile);
        private static string BkpTemplate => DateTime.Now.ToString("MM.dd.yyyy_HH-mm-ss") + ".json";
        public static JsonSettings JsonConfig = new();
        public static JsonLang JsonLanguage = new();

#pragma warning restore CA2211 // Non-constant fields should not be visible

        public static void LoadJson(JsonType type)
        {
            Log($"Loading {type}");
            var path = string.Empty;
            var file = string.Empty;
            var fullpath = string.Empty;
            try
            {
                switch (type)
                {
                    case JsonType.Cfg:
                        path = CfgPath;
                        file = CfgFile;
                        fullpath = Path.Combine(path, file);
                        JsonConfig = JsonConvert.DeserializeObject<JsonSettings>(File.ReadAllText(CfgFullPath)) ?? new();
                        break;
                    case JsonType.Lang:
                        path = LangPath;
                        file = LangFile;
                        fullpath = Path.Combine(path, file);
                        JsonLanguage = JsonConvert.DeserializeObject<JsonLang>(File.ReadAllText(LangFullPath)) ?? new();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(type), type, null);
                }
            }

            catch (DirectoryNotFoundException)
            {
                Log($"Path \"{path}\" not found, creating and reloading...");
                Directory.CreateDirectory(path);
                LoadJson(type);
                return;
            }
            catch (FileNotFoundException)
            {
                Log($"{type} file({file}) not found.\n>>>Creating new one.");
                SaveJson(type);
            }
            catch (JsonReaderException)
            {
                Log($"{type} file({fullpath}) is broken.\n>>>Resetting to defaults.\n>>>Backup of broken {type} saved to {type + BkpTemplate}", LogType.Warn);
                try
                {
                    File.Move(fullpath, type + BkpTemplate);
                }
                catch
                {
                    Log($"Unable to backup {type} file.", LogType.Error);
                }
                SaveJson(type);
            }
            Log($"{type} loaded.");
        }
        public static void SaveJson(JsonType type)
        {
            try
            {
                Log($"Saving {type}");
                switch (type)
                {
                    case JsonType.Cfg:
                        File.WriteAllText(CfgFullPath, JsonConvert.SerializeObject(JsonConfig, Formatting.Indented));
                        break;
                    case JsonType.Lang:
                        File.WriteAllText(LangFullPath, JsonConvert.SerializeObject(JsonLanguage, Formatting.Indented));
                        break;
                }
                Log($"{type} saved successfuly.");

            }
            catch (Exception e)
            {
                Log($"Exception caught on saving {type}\n>>>" + e.Message);
            }
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using TelegramBotWPF.Classes;
using static TelegramBotWPF.Classes.LogUtil;
using static TelegramBotWPF.Models.JsonModel;

#nullable enable

namespace TelegramBotWPF.Models
{
    public class Track
    {
        public string? Artist;
        public string? Title;
        public string? Duration;
        public string? Genre;
        public string? Description;
        public string? StartTime;
        public string? TrackIndex;
        public string Name => FormatTrackInfo(Artist, Title);

        public static string FormatTrackInfo(string? artist, string? title)
        {
            artist ??= string.Empty;
            title ??= string.Empty;
            return artist.Length switch
            {
                > 0 when title.Length > 0 => artist + " - " + title,
                > 0 when title.Length == 0 => artist,
                0 when title.Length > 0 => title,
                _ => "Unknown"
            };
        }
    }

    internal class RadioBossModel
    {
        private readonly string? _radioBossPassword;
        private readonly string? _radioBossLink;
        private XDocument? _data;

        private readonly XmlReaderSettings _xmlReaderSettings = new()
        {
            Async = true
        };

        public Track? CurrentTrack;
        public Track? NextTrack;
        public string NowListening => GetParameter("CurrentTrack", "LISTENERS");
        private const int CutLength = 200;
        public MemoryStream ArtWork => GetArtwork();
        public bool ConnectionValid;

        /// <summary>
        /// Getting info from radioboss API
        /// Info stored in private variable and can be safely accessed via public methods
        /// </summary>
        public async Task RetrieveData()
        {
            try
            {
                _data = await DoRequest("playbackinfo");
                CurrentTrack = WriteTrackInfo();
                NextTrack = WriteTrackInfo(true);
                if (ConnectionValid)
                {
                    return;
                }
                Log("RadioBoss data received.");
                ConnectionValid = true;
            }
            catch (Exception e)
            {
                Log($"Error occurred on receiving RadioBoss data.\n>>>{e.Message}", LogType.Error);
                ConnectionValid = false;
            }
        }

        private Track WriteTrackInfo(bool useNextTrackInfo = false)
        {
            Track track = new();
            string type = useNextTrackInfo ? "NextTrack" : "CurrentTrack";
            string commstr = GetParameter(type, "COMMENT");
            track.Artist = GetParameter(type, "ARTIST");
            track.Title = GetParameter(type, "TITLE");
            track.Duration = GetParameter(type, "DURATION");
            track.Genre = GetParameter(type, "GENRE");
            track.Description = commstr.Length > CutLength ? commstr[..CutLength] + "..." : commstr.Length == 0 ? "n/a" : commstr;
            return track;
        }

        private async Task<XDocument> DoRequest(string action) => await XDocument.LoadAsync(XmlReader.Create(_radioBossLink + @"/?pass=" + _radioBossPassword +
                                                              @"&action=" + action, _xmlReaderSettings), LoadOptions.None, CancellationToken.None);

        private string GetParameter(string element, string attribute)
        {
            try
            {
                var xDoc = _data;
                if (xDoc == null)
                {
                    return string.Empty;
                }
                foreach (XElement xElem in xDoc.Element("Info")!.Element(element)!.Elements("TRACK"))
                {
                    return xElem.Attribute(attribute)!.Value;
                }
            }
            catch (Exception e)
            {
                Log($"Unable to get parameter ({element}/{attribute})\n>>>{e.Message}", LogType.Error);
            }

            return string.Empty;
        }

        private string GetParameter(string attribute)
        {
            try
            {
                var xDoc = _data;
                if (xDoc == null)
                {
                    return string.Empty;
                }
                foreach (XElement xElem in xDoc.Element("Info")!.Elements("Playback"))
                {
                    return xElem.Attribute(attribute)!.Value;
                }
            }
            catch (Exception e)
            {
                Log($"Unable to get parameter ({attribute})\n>>>{e.Message}", LogType.Error);
            }

            return string.Empty;
        }

        /// <summary>
        /// Getting time, when next track will be played
        /// </summary>
        /// <returns>
        /// String formatted to HH:mm:ss with time to next track
        /// </returns>
        public string NextTrackDate()
        {
            try
            {
                var lastTime = Convert.ToDateTime(GetParameter("CurrentTrack", "LASTPLAYED"));
                var timeToNext = TimeSpan.FromMilliseconds(int.Parse(GetParameter("len")));

                lastTime = lastTime.Add(timeToNext);
                return lastTime.ToString("HH:mm:ss");
            }
            catch (Exception e)
            {
                Log($"Cant parse next track time\n>>>{e.Message}", LogType.Error);
                return "?";
            }
        }

        public async Task<int> CurrentTrackIndex()
        {
            await RetrieveData();
            return int.Parse(GetParameter("playlistpos")) - 1;
        }

        public async Task<List<Track>> CurrentPlaylist()
        {
            var result = new List<Track>();
            var data = await DoRequest("getplaylist2");
            try
            {
                result.AddRange(data.Element("Playlist")!.Elements("TRACK")
                .Select(xElem => new Track()
                {
                    Title = xElem.Attribute("TITLE")!.Value,
                    Artist = xElem.Attribute("ARTIST")!.Value,
                    Duration = xElem.Attribute("DURATION")!.Value,
                    Genre = xElem.Attribute("GENRE")!.Value,
                    StartTime = xElem.Attribute("STARTTIME")!.Value,
                    TrackIndex = xElem.Attribute("PLAYLISTINDEX")!.Value
                }));
            }
            catch (Exception e)
            {
                Log($"Unable to get track info\n>>>{e.Message}", LogType.Error);
            }
            return result;
        }

        /// <summary>
        /// Requests artwork from radioboss
        /// </summary>
        /// <returns>
        /// Returns MemoryStream with artwork bytes
        /// </returns>
        private MemoryStream GetArtwork()
        {
            MemoryStream artwork = new();
            try
            {
                using HttpClient httpClient = new();
                artwork = new MemoryStream(httpClient.GetByteArrayAsync(
                    _radioBossLink + @"/?pass=" + _radioBossPassword + "&action=trackartwork").Result);
            }
            catch (Exception e)
            {
                Log($"Cant get artwork.\n>>>{e.Message}", LogType.Error);
            }

            return artwork;
        }

        public RadioBossModel(bool ready = true)
        {
            if (ready)
            {
                _radioBossPassword = JsonConfig.RadioBossPass;
                if (_radioBossPassword == string.Empty)
                {
                    Log("RadioBoss password is empty", LogType.Warn);
                }
                _radioBossLink = JsonConfig.RadioBossUrl;
                if (_radioBossLink == string.Empty)
                {
                    Log("RadioBoss API link is empty", LogType.Warn);
                }
            }
        }
    }
}
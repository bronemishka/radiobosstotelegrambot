﻿namespace TelegramBotWPF.Models
{
    //internal class BotMessageModel
    //{
    //    public string RadioName = "Vandal FM";
    //    private string _audioMessage = $"На %radioName% 🔊\n\n▶ Сейчас:\n%currTrack% [%currTrackLen%]\n%genre%\nИнфо: %comment%\n\n⏭ Следующий в %nextTrackDate% MSK\n%nextTrack% [%nextTrackLen%]\n%nextgenre%\nИнфо: %nextcomment%\n\n👤 Слушателей: %listeners%";
    //
    //    public string AudioMessage
    //    {
    //        get => _audioMessage;
    //        set => _audioMessage = value;
    //    }
    //
    //    public string StatusMessage = "";
    //    public string ListenUrlText = "Слушать %currTrack% на %radioName%";
    //}
    internal struct MessagePrefix
    {
        public static readonly string
            radioName = "%radioName%",
            listeners = "%listeners%",
            currTrack = "%currTrack%",
            currTrackLen = "%currTrackLen%",
            nextTrack = "%nextTrack%",
            nextTrackDate = "%nextTrackDate%",
            nextTrackLen = "%nextTrackLen%",
            genre = "%genre%",
            nextgenre = "%nextgenre%",
            comment = "%comment%",
            nextcomment = "%nextcomment%";
    }
}